import {Tagger} from "./tagger";

describe('Tagger', () => {
    let tagger: Tagger;
    beforeEach(() => {
        tagger = new Tagger();
    });

    it('should add tags to method', () => {
         expect(tagger.tag('foo')).toBe('foo');
    });
});
