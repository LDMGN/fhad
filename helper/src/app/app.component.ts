import {Component} from '@angular/core';
import {DomSanitizer} from "@angular/platform-browser";
import {Line, Tagger, WordPart} from "./tagger";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    input: string = '';
    lines: Line[] = [];

    constructor(private sanitizer: DomSanitizer, private tagger: Tagger) {
    }

    loadExample() {
        this.input = `GET /examples/Zm9vYmFy?query=foo HTTP/1.1
User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)
Host: wedonthaveawebsite.nl
Accept-Language: en-us
Accept-Encoding: gzip, deflate
Connection: Keep-Alive`;
        this.updateLines();
    }

    updateLines() {
        this.lines = this.input
            .replace("\r\n", "\n")
            .split("\n")
            .filter(line => line.length)
            .map(line => this.tagger.tag(line));
    }

    addComment(part: WordPart) {
        part.comment = 'Foo';
    }

    wordPartsWithComments() {
        const results: WordPart[] = [];
        this.lines
            .forEach(line =>
                line.words
                    .forEach(word =>
                        word.parts()
                            .filter(part => part.hasComment())
                            .forEach(part => results.push(part))));
        return results;
    }
}
