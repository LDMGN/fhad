export class Line {
    constructor(public words: Word[]) {
    }
}

export class Word {
    private wordParts: WordPart[];

    constructor(public text: string, public trailingSpace: boolean, private line: string) {
        this.wordParts = this.decomposeWord();
    }

    private decomposeWord() {
        if (this.isUrl(this.text) && this.startsWithHttpMethod(this.line)) {
            return this.addUrlParts(this.text
                .split('/')
                .filter(part => part.length)
                .map(part => new WordPart(part, true)));
        }
        return [new WordPart(this.text, true)];
    }

    private addUrlParts(parts: WordPart[]): WordPart[] {
        const urlPart = new WordPart('/', false);
        const result = [];
        parts.forEach(part => {
            result.push(urlPart);
            result.push(part);
        });
        return result;
    }

    private startsWithHttpMethod(text: string) {
        return ['GET', 'HEAD', 'POST', 'PUT', 'PATCH', 'DELETE']
            .filter(method => text.startsWith(method))
            .length > 0;
    }

    private isUrl(text: string) {
        return text.startsWith('/');
    }

    public parts(): WordPart[] {
        return this.wordParts;
    }
}

export class WordPart {
    public comment: string;

    constructor(public text: string, public hasTag: boolean) {
    }

    public hasComment() {
        return this.comment != null;
    }
}

export class Tagger {
    public tag(line: string) {
        const lineParts = line.split(' ');
        const wordCount = lineParts.length;
        const words = lineParts
            .map((word, index) => new Word(word, index < wordCount - 1, line));
        return new Line(words);
    }
}
