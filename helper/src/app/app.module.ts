import {CommonModule} from "@angular/common";
import {NgModule} from '@angular/core';
import {FormsModule} from "@angular/forms";
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {Tagger} from "./tagger";
import { ArrowComponent } from './arrow/arrow.component';

@NgModule({
    declarations: [
        AppComponent,
        ArrowComponent
    ],
    imports: [
        BrowserModule,
        CommonModule,
        FormsModule
    ],
    providers: [
        Tagger
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {
}
