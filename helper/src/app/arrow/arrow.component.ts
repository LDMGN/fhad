import {Component, Input} from '@angular/core';

@Component({
    selector: 'app-arrow',
    templateUrl: './arrow.component.html',
    styleUrls: ['./arrow.component.css']
})
export class ArrowComponent {
    @Input()
    private direction: string;

    getViewBox() {
        return `0 0 ${this.width()}, ${this.height()}`;
    }

    width(): number {
        return 400;
    }

    height(): number {
        return 100;
    }

    color(): string {
        return 'red';
    }

    getPath(): string {
        const width = this.width();
        const height = this.height();
        if (this.arrowGoesUp()) {
            return `M 0 ${height} C 0 ${height * .25} ${width * .5} 1 ${width} 1`;
        }
        return `M 0 0 C 0 ${height * .75} ${width * 0.5} ${height * .9} ${width} ${height * .9}`;
    }

    private arrowGoesUp() {
        return this.direction === 'up';
    }
}
